<?php

/**
 *
 */

function fcc_sandbox_admin_settings() {
	$form = array();
	$options = array(
		0 => t('No advert'),
		1 => t('One advert'),
		2 => t('Two adverts'),
		10 => t('Ten adverts'),
		1000 => t('Too many adverts'),
		);
	$form['number_of_frontpage_ads'] = array(
		'#type' => 'select',
		'#title' => t('Number of frontpage ads'),
		'#description' => t('Select how many ads will be shown on the front page'),
		'#options' => $options,
		'#default_value' => variable_get('number_of_frontpage_ads', 0),
		);
	
	return system_settings_form($form);
}

?>
